declare module Express {
  interface Session { /* extend express-session */
    user: {
      id: string;
      username: string;
    }
  }

  interface Request {
    correlationId?: string;

    safeBody?: any;
    dangerouslyUnsafeBody?: object;
    safeQuery?: any;
    dangerouslyUnsafeQuery?: object;
    safeParams?: any;
    dangerouslyUnsafeParams?: object;
  }
}
