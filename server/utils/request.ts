import { NextFunction, Request, RequestHandler, Response } from 'express';
import * as D from 'io-ts/Decoder'
import { isLeft } from 'fp-ts/Either'


import { ValidationDecodeError } from './errors';


export interface ISafeBodyRequest<T> extends Request {
  safeBody: Readonly<T>;
  safeQuery: null;
  safeParams: null;
}

export interface ISafeParamsRequest<T> extends Request {
  safeBody: null;
  safeQuery: null;
  safeParams: Readonly<T>;
}

export interface ISafeQueryRequest<T> extends Request {
  safeBody: null;
  safeQuery: Readonly<T>;
  safeParams: null;
}

export interface ISafeParamsQueryRequest<T, U> extends Request {
  safeBody: null;
  safeQuery: U;
  safeParams: T;
}

export interface ISafeBodyParamsRequest<T, U> extends Request {
  safeBody: Readonly<T>;
  safeQuery: null;
  safeParams: Readonly<U>;
}

export interface ISafeBodyQueryRequest<T, U> extends Request {
  safeBody: Readonly<T>;
  safeQuery: Readonly<U>;
  safeParams: null;
}


export interface ISafeBodyParamsQueryRequest<T, U, V> extends Request {
  safeBody: Readonly<T>;
  safeParams: Readonly<U>;
  safeQuery: Readonly<V>;
}

export const decode = <I, A>(Decoder: D.Decoder<I, A>, input: I): A => {
  const result = Decoder.decode(input);
  if (isLeft(result)) {
    const message = D.draw(result.left);
    throw new ValidationDecodeError(message);
  }
  return result.right;
}

interface IValidator {
  body?: any;
  params?: any;
  query?: any;
  form?: any;
}

export const validation = ({ body: BodyDecoder, query: QueryDecoder, params: ParamsDecoder, form: FormDecoder }: IValidator): RequestHandler => async (
  req: Request,
  _: Response,
  next: NextFunction
) => {
  try {
    if (BodyDecoder) {
      req.safeBody = decode(BodyDecoder, req.body);
    }

    if (QueryDecoder) {
      req.safeQuery = decode(QueryDecoder, req.query);
    }

    if (ParamsDecoder) {
      req.safeParams = decode(ParamsDecoder, req.params);
    }

    if (FormDecoder) {
      req.safeBody = decode(FormDecoder, JSON.parse(req.body?.payload ?? '{}'));
    }

    req.dangerouslyUnsafeBody = req.body;
    req.body = null;

    req.dangerouslyUnsafeQuery = req.query;
    req.query = {};

    req.dangerouslyUnsafeParams = req.params;
    req.params = {};
  
  } catch (error) {
    return next(error);
  }

  return next();
};

export type TSafeRequest =
  ISafeQueryRequest<any> & 
  ISafeParamsRequest<any> &
  ISafeBodyRequest<any> &
  ISafeParamsQueryRequest<any, any> &
  ISafeBodyParamsRequest<any, any> &
  ISafeBodyQueryRequest<any, any> &
  ISafeBodyParamsQueryRequest<any, any, any>;

export type TSafeRequestHandler = (req: TSafeRequest, res: Response, next: NextFunction) => Promise<any>;

export const asyncSafeHandler = (fn: TSafeRequestHandler) => (req: Request, res: Response, next: NextFunction) => fn(req as TSafeRequest, res, next).catch(next);
