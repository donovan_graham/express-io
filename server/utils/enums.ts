import * as t from 'io-ts';

export enum CurrencyCode {
  ZAR = 'ZAR',
  EUR = 'EUR',
  USD = 'USD',
  GBP = 'GBP',
  JPY = 'JPY',
  AUD = 'AUD',
}

export const CurrencyCodes = t.keyof({
  [CurrencyCode.ZAR]: null,
  [CurrencyCode.EUR]: null,
  [CurrencyCode.USD]: null,
  [CurrencyCode.GBP]: null,
  [CurrencyCode.JPY]: null,
  [CurrencyCode.AUD]: null,
});
