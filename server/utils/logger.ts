import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';

import { ApplicationError } from './errors';

export enum LogLevel {
  Panic = 'panic', // 0
  Alert = 'alert', // 1
  Critical = 'crit', // 2
  Error = 'error', // 3
  Warn = 'warn', // 4
  Notice = 'notice', // 5
  Info = 'info', // 6
  Debug = 'debug', // 7
}

enum LogType {
  Error = 'error',
  Request = 'request',
  Response = 'response',
}

const loggerMap: { [k in LogLevel]: 'error' | 'warn' | 'info' | 'debug' } = {
  [LogLevel.Panic]: 'error',
  [LogLevel.Alert]: 'error',
  [LogLevel.Critical]: 'error',
  [LogLevel.Error]: 'error',
  [LogLevel.Warn]: 'warn',
  [LogLevel.Notice]: 'info',
  [LogLevel.Info]: 'info',
  [LogLevel.Debug]: 'debug',
};

const sizeLimit = 1024 * 4; // 4kb

const slice = (payload: any, limit: number = sizeLimit) => Buffer.from(JSON.stringify(payload), 'utf8').toString('utf8', 0, limit);

const prettyStack = (error: ApplicationError) =>
  // @ts-ignore
  error.stack
    .toString()
    .split('\n')
    .filter((s: string) => !s.includes('(<anonymous>)'))
    .map((s: string) => s.replace('/service/server/', ''))
    .join('\n');

const traceInfo = (request: Request) => ({
  correlation_id: request.correlationId,
});

const requestInfo = (request: Request) => ({
  // request_headers: request.headers,
  method: request.method,
  path: `${request.baseUrl}${request.path}`,
  query_string: request.query,
  request_content_type: request.get('content-type'),
  request_body: request.body ? slice(request.body) : '',
  remote_ip: request.ip || request.connection?.remoteAddress || request.socket?.remoteAddress,
  remote_port: request.connection?.remotePort || request.socket?.remotePort,
});

const errorInfo = (error: ApplicationError | Error) => {
  if (error instanceof ApplicationError) {
    return {
      timestamp: error.timestamp,
      error_name: error.name,
      full_message: error.stack ? prettyStack(error) : JSON.stringify(error),
      meta: error.meta ? slice(error.meta) : undefined,
    };
  }

  return {
    timestamp: Date.now(),
    error_name: error.name,
    full_message: JSON.stringify(error),
  };
};

export const requestLogger = (request: Request, response: Response, next: NextFunction) => {
  if (request.is('multipart/form-data') === 'multipart/form-data') {
    return next();
  }

  const start = process.hrtime.bigint();
  // tslint:disable-next-line
  console.log(
    JSON.stringify({
      level_name: LogLevel.Info,
      type: LogType.Request,
      timestamp: Date.now(),
      full_message: `Request ${request.method} ${request.url}`,
      ...traceInfo(request),
      ...requestInfo(request),
    })
  );

  response.on('finish', () => {
    console.log(
      JSON.stringify({
        level_name: response.statusCode < 400 ? LogLevel.Info : LogLevel.Warn,
        type: LogType.Response,
        timestamp: Date.now(),
        full_message: `Response ${response.statusCode} to ${request.method} ${request.url}`,
        response_status_code: response.statusCode,
        response_body_size: response.get('Content-Length'),
        duration: ((process.hrtime.bigint() - start) / BigInt(1000000)).toString(),
        ...traceInfo(request),
        ...requestInfo(request),
      })
    );
  });

  next();
};

export const errorLogger = (error: ApplicationError | Error, request: Request) => {
  const logger = error instanceof ApplicationError ? console[loggerMap[error.logLevel]] : console.error;

  logger(
    JSON.stringify({
      level_name: error instanceof ApplicationError ? error.logLevel : LogLevel.Error,
      type: LogType.Error,
      ...traceInfo(request),
      ...requestInfo(request),
      ...errorInfo(error),
    })
  );
};

export const errorHandler = (error: ApplicationError | Error, request: Request, response: Response, next: NextFunction) => {
  errorLogger(error, request);

  if (error instanceof ApplicationError) {
    return response.status(error.responseStatusCode).end();
  }

  return response.status(StatusCodes.INTERNAL_SERVER_ERROR).end();
};