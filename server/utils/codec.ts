import * as D from 'io-ts/Decoder'
import { pipe } from 'fp-ts/function'

export const BooleanFromString: D.Decoder<unknown, boolean> = pipe(
  D.string,
  D.parse((s: string) => {
    switch (s) {
      case 'true':
        return D.success(true)
      case 'false':
        return D.success(false)
      default:
        return D.failure(s, 'BooleanFromString');
    }
  }),
);

export const NumberFromString: D.Decoder<unknown, number> = pipe(
  D.string,
  D.parse((s: string) => {
    const n = parseFloat(s);
    return isNaN(n) ? D.failure(s, 'NumberFromString') : D.success(n);
  }),
);

export const ArrayFromString = <A>(item: D.Decoder<unknown, A>, deliminator?: string): D.Decoder<unknown, Array<A>> => pipe(
  D.string,
  D.parse((s: string) => {
    const arr = s.split(deliminator || ',');
    return D.array(item).decode(arr)
  })
);

// Examples: 
export const ArrayOfNumbersFromString = ArrayFromString(D.number);
export const ArrayOfStringsFromString = ArrayFromString(D.string);


/* BRANDS */
export interface PositiveBrand {
  readonly Positive: unique symbol
}
export type Positive = number & PositiveBrand;
export const Positive: D.Decoder<unknown, Positive> = pipe(
  D.number,
  D.refine((n: number): n is Positive => n > 0, 'Positive')
);


export const reSafeString = /^[A-Z0-9\-\+\_\s]+$/i;
export interface SafeStringBrand {
  readonly SafeString: unique symbol
}
export type SafeString = string & SafeStringBrand;
export const SafeString: D.Decoder<unknown, SafeString> = pipe(
  D.string,
  D.refine((s: string): s is SafeString => s.length >= 1 && s.length <= 255 && reSafeString.test(s), 'SafeString')
);


export const reSimpleDateString = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/;
export interface SimpleDateStringBrand {
  readonly SimpleDateString: unique symbol
}
export type SimpleDateString = string & SimpleDateStringBrand;
export const SimpleDateString: D.Decoder<unknown, SimpleDateString> = pipe(
  D.string,
  D.refine((s: string): s is SimpleDateString => s.length === 10 && reSimpleDateString.test(s), 'SimpleDateString')
);


const reGuid = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i
export interface GuidBrand {
  readonly Guid: unique symbol
}
export type Guid = string & GuidBrand;
export const Guid: D.Decoder<unknown, Guid> = pipe(
  D.string,
  D.refine((s: string): s is Guid => s.length === 36 && reGuid.test(s), 'Guid'),
);


export const reEmail = /^$|^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/;
export interface EmailBrand {
  readonly Email: unique symbol
}
export type Email = string & EmailBrand;
export const Email: D.Decoder<unknown, Email> = pipe(
  D.string,
  D.refine((s: string): s is Email => s.length >= 1 && s.length <= 255 && reEmail.test(s), 'Email')
);

export interface NonEmptyArrayBrand {
  readonly NonEmptyArray: unique symbol
}
export type NonEmptyArray<A> = A[] & NonEmptyArrayBrand;
export const NonEmptyArray = <A>(item: D.Decoder<unknown, A>): D.Decoder<unknown, Array<A>> =>
  pipe(
    D.array(item),
    D.refine((arr: Array<A>): arr is NonEmptyArray<A> => arr.length > 0, 'NonEmptyArray')
  )

