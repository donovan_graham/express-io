import { Response } from 'express';
import * as D from 'io-ts/Decoder'
import { pipe } from 'fp-ts/function'

import { BooleanFromString } from '../utils/codec';
import { validation, asyncSafeHandler, ISafeQueryRequest } from '../utils/request';

const QueryRequiredParams = D.struct({
 name: D.string,
 active: BooleanFromString,
});

const QueryOptionalParams = D.partial({
  age: D.number,
});

export const QueryParams = pipe(
  QueryRequiredParams,
  D.intersect(QueryOptionalParams)
);

type TQueryParams = D.TypeOf<typeof QueryParams>;

const validate = validation({ query: QueryParams });

const route = async (req: ISafeQueryRequest<TQueryParams>, res: Response) => {
  return res.json({
    name: req.safeQuery.name,
    active: req.safeQuery.active === true ? 'yes' : 'no',
  });
};

export default [validate, asyncSafeHandler(route)];
