import express, { Express, Request, Response } from 'express';
import helmet from 'helmet';
import { StatusCodes } from 'http-status-codes';

import { requestLogger, errorHandler } from './utils/logger';

import helloRoute from './routes/hello';

const PORT = process.env.PORT || 3000;

const app: Express = express();

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/ping', (_: Request, res: Response) => res.status(StatusCodes.OK).send('pong'));

app.use(requestLogger);

app.get('/hello', helloRoute);

app.get('/', (req: Request, res: Response) => {
  res.send('<html><body><h1>Hello World!</h1></body></html>');
});

app.get('*', (_: Request, res: Response) => res.status(StatusCodes.NOT_FOUND).end());

app.use(errorHandler);



app.listen(PORT, () => console.log(`Running on ${PORT} ⚡`));