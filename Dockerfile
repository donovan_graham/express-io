FROM node:16-alpine3.11


ENV NODE_ENV=production
EXPOSE 3000
WORKDIR /server

COPY server /server/
RUN npm install --production --no-audit

USER nobody
CMD node index.js